package ro.emag.hackathon.bewearhost;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class Activity2 extends Activity {

    public static TextView data;

    String USER_LATITUDE;
    String USER_LONGITUDE;

    String[] USERNAMES = {"Rares Serea"};
    String[] NOTIFICATIONS = {"Alert"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);

//        click = findViewById(R.id.alerts);
//        data = findViewById(R.id.fetchedData);
//
//        click.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                fetchData process = new fetchData();
//                process.execute();
//            }
//        });

        ListView listView= findViewById(R.id.listView);

        CustomAdapter customAdapter = new CustomAdapter();
        listView.setAdapter(customAdapter);

    }

    class CustomAdapter extends BaseAdapter {
        private Button buttonMaps;
        @Override
        public int getCount() {
            return USERNAMES.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = getLayoutInflater().inflate(R.layout.customlayout, null);

            TextView textView_username =convertView.findViewById(R.id.usernameAlert);
            TextView textView_notificationType = convertView.findViewById(R.id.notificationType);

            textView_username.setText(USERNAMES[position]);
            textView_notificationType.setText(NOTIFICATIONS[position]);

            Intent myIntent = getIntent();
            final String latitude = myIntent.getStringExtra("latitude");
            final String longitude = myIntent.getStringExtra("longitude");
//            Log.d("[map]",latitude+latitude);

            buttonMaps = convertView.findViewById(R.id.buttonMaps);
            buttonMaps.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    openMapsActivity(Double.parseDouble(latitude), Double.parseDouble(longitude));
//                    Log.d("[maponclick]",latitude+latitude);

                }
            });

            return convertView;
        }
    }

    public void openMapsActivity(double latitude, double longitude) {
        Intent intent = new Intent(this, MapsActivity.class);

        intent.putExtra("latitude", latitude);
        intent.putExtra("longitude", longitude);

        startActivity(intent);
    }
}
