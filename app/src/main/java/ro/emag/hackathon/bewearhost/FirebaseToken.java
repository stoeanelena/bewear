package ro.emag.hackathon.bewearhost;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class FirebaseToken extends FirebaseInstanceIdService {
    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("[Token]", "Refreshed token: " + refreshedToken);
        final SendToken sendToken = new SendToken();
    }
}


