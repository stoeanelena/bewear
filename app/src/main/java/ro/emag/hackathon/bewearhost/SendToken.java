package ro.emag.hackathon.bewearhost;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

class SendToken extends AsyncTask<String, String, String> {

    private final String USER_AGENT = "Mozilla/5.0";

    @Override
    protected String doInBackground(String... params) {
        try {
            String url = "http://104.248.45.34/save-token";
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            //add reuqest header
            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

            String urlParameters = "notificationToken=" + params[0] + "&userId" + "36287";

            // Send post request
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            //        wr.writeBytes(jsonObject.toString());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();

            Log.d("[UrlParam]", urlParameters);

            int responseCode = con.getResponseCode();
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            Log.d("[Response]", response.toString());

            //log result
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }


}