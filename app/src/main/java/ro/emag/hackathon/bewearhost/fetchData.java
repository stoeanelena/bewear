package ro.emag.hackathon.bewearhost;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class fetchData extends AsyncTask<Void, Void, Void>
{
    String data = "";
    String dataParsed = "";
    String singleParsed = "";
    JSONObject coordinates = null;

    @Override
    protected Void doInBackground(Void... voids) {
        try {
            URL url = new URL("https://api.myjson.com/bins/1765eo");

            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            InputStream inputStream = httpURLConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";

            while (line != null) {
                line = bufferedReader.readLine();
                data = data + line;
            }

            JSONArray JA = null;
            try {
                JA = new JSONArray(data);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            for (int i=0; i<JA.length(); i++) {
                JSONObject JO = null;
                try {
                    JO = (JSONObject) JA.get(i);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                    coordinates = JO.getJSONObject("coordinates");

                    Log.i("ceva", coordinates.toString());

                    Object latitude = coordinates.get("latitude");
                    Object longitude = coordinates.get("longitude");

                    singleParsed = "Username: " + JO.get("username") + "\n" +
                            "Notification: " + JO.get("notification") + "\n" +
                            "Latitude: " + latitude + "\n" +
                            "Latitude: " + longitude + "\n";
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                dataParsed = dataParsed + singleParsed + "\n";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        Activity2.data.setText(this.dataParsed);
    }
}
