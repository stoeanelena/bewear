package ro.emag.hackathon.bewearhost;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class NotificationService extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d("[remoteMessage]", "From: " + remoteMessage.getFrom());
        if (remoteMessage.getData().size() > 0) {
            Log.d(remoteMessage.getNotification().getBody(), "Message data payload: " + remoteMessage.getData());
            this.showNotification(remoteMessage.getNotification().getTitle(),remoteMessage.getNotification().getBody());

            Intent intent = new Intent(getBaseContext(), Activity2.class);
            intent.putExtra("latitude", remoteMessage.getData().get("latitude"));
            intent.putExtra("longitude", remoteMessage.getData().get("longitude"));

            Log.d("[map2]",remoteMessage.getData().get("latitude").getClass().getName()+remoteMessage.getData().get("longitude").getClass().getName());


            startActivity(intent);

        }
        if (remoteMessage.getNotification() != null) {
            Log.d("[remoteMessage]", "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

    }

    public void onNewToken(String token) {
        Log.d("[remoteMessage]", "Refreshed token: " + token);
    }

    public void showNotification(String title, String content) {

//        // Create an Intent for the activity you want to start
//        Intent resultIntent = new Intent(this, Activity2.class);
//
//        resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
//                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//// Create the TaskStackBuilder and add the intent, which inflates the back stack
//        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
//        stackBuilder.addNextIntentWithParentStack(resultIntent);
//// Get the PendingIntent containing the entire back stack
//        PendingIntent resultPendingIntent =
//                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
//
//        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "default")
//                .setSmallIcon(R.mipmap.ic_launcher_round) // notification icon
//                .setContentTitle(title) // title for notification
//                .setContentText(content)// message for notification
////                .setSound(alarmSound) // set alarm sound for notification
//                .setAutoCancel(true); // clear notification after click
//
//        builder.setContentIntent(resultPendingIntent);
//        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
//        notificationManager.notify(0, builder.build());
//
//        Intent notifyIntent = new Intent(this, Activity2.class);
//// Set the Activity to start in a new, empty task
//        notifyIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
//                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//// Create the PendingIntent
//        PendingIntent notifyPendingIntent = PendingIntent.getActivity(
//                this, 0, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT
//        );
//
//        NotificationCompat.Builder nbuilder = new NotificationCompat.Builder(this, "default")
//                .setSmallIcon(R.mipmap.ic_launcher_round) // notification icon
//                .setContentTitle(title) // title for notification
//                .setContentText(content)// message for notification
////                .setSound(alarmSound) // set alarm sound for notification
//                .setAutoCancel(true); // clear notification after click
//
//        nbuilder.setContentIntent(notifyPendingIntent);
//        NotificationManagerCompat nnotificationManager = NotificationManagerCompat.from(this);
//        nnotificationManager.notify(0, builder.build());











//
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("default",
                    "YOUR_CHANNEL_NAME",
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription("YOUR_NOTIFICATION_CHANNEL_DISCRIPTION");
            mNotificationManager.createNotificationChannel(channel);
        }
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext(), "default")
                .setSmallIcon(R.mipmap.ic_launcher_round) // notification icon
                .setContentTitle(title) // title for notification
                .setContentText(content)// message for notification
//                .setSound(alarmSound) // set alarm sound for notification
                .setAutoCancel(true); // clear notification after click
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        PendingIntent pi = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(pi);
        mNotificationManager.notify(0, mBuilder.build());
    }
}
