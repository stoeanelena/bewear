package ro.emag.hackathon.bewearhost;

import org.json.JSONObject;

public class Alert {
    String username;
    String notification;
    JSONObject coordinates;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNotification() {
        return notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }

    public JSONObject getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(JSONObject coordinates) {
        this.coordinates = coordinates;
    }
}
